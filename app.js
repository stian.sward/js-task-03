const fs = require('fs');
const puppeteer = require('puppeteer');
const url = 'https://www.worldometers.info/coronavirus/';
const selector = '#maincounter-wrap';
const filename = 'output.json';


async function getData() {
    const data = {};
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(url);
    const list = await page.$$eval(selector, nodes => {
        return nodes.map(node => {
            const h1 = node.querySelector('h1').textContent.split(' ');
            const key = h1[h1.length - 1];
            const val = node.querySelector('span').textContent;
            return {
                key: key,
                val: val
            };
        })
    });
    list.forEach(pair => {
        data[pair.key] = pair.val;
    })
    await browser.close();
    return data;
};

console.log('Fetching data from', url, '...');
getData().then((res) => {
    fs.writeFile(filename, JSON.stringify(res, null, "\t"), (err) => {
        if (err) throw err;
        console.log('Results written to', filename);
    });
});